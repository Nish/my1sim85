# makefile for my1sim85 - 8085 system simulation and development tool

PROJECT = my1sim85
GUISPRO = $(PROJECT)
GUISOBJ = my1i8085.o my1sim85.o my1comlib.o wxterm.o
GUISOBJ += wxbit.o wxled.o wxswitch.o wxcode.o wxpanel.o
GUISOBJ += wxpref.o wxform.o wxmain.o

EXTPATH = ../my1asm85/src
EX2PATH = ../my1termu/src
EX3PATH = ../my1termw/src
PACKDIR = $(PROJECT)-$(shell cat VERSION)
PACKDAT = README TODO CHANGELOG VERSION asm sys
PLATBIN ?= $(shell uname -m)
VERSION = -DMY1APP_PROGVERS=\"$(shell date +%Y%m%d)\"

DELETE = rm -rf
COPY = cp -R
ARCHIVE = tar cjf
ARCHEXT = .tar.bz2
CONVERT = convert

CFLAGS += -Wall -I$(EXTPATH) -I$(EX2PATH) -I$(EX3PATH)
LFLAGS +=
OFLAGS +=
LOCAL_FLAGS =
WX_LIBS = stc,aui,html,adv,core,xml,base

ifeq ($(DO_MINGW),YES)
	GUISPRO = $(PROJECT).exe
	GUISOBJ += wxmain.res
	PLATBIN = mingw
	ARCHIVE = zip -r
	ARCHEXT = .zip
	# cross-compiler settings
	XTOOL_DIR ?= /home/share/tool/xtool-mingw
	ifneq ("$(wildcard $(XTOOL_DIR)/bin/*-gcc)","")
		XTOOL_TARGET = $(XTOOL_DIR)
	else
		XTOOL_TARGET = /usr
	endif
	XTOOL_PREFIX ?= x86_64-w64-mingw32-
	BUILD_TOOL=$(shell which $(XTOOL_PREFIX)gcc)
	CROSS_COMPILE = $(XTOOL_TARGET)/bin/$(XTOOL_PREFIX)
	# below is to remove console at runtime
	LFLAGS += -Wl,-subsystem,windows
	# extra switches
	CFLAGS += --static
	CFLAGS += -I$(XTOOL_DIR)/include -DDO_MINGW -DWIN32_LEAN_AND_MEAN
	LFLAGS += -L$(XTOOL_DIR)/lib
	# -mthreads is not playing nice with others - has to go!
	WXCFG_BIN = $(XTOOL_DIR)/bin/wx-config
	WX_LIBFLAGS = $(shell $(WXCFG_BIN) --libs $(WX_LIBS) | sed 's/-mthreads//g')
	WX_CXXFLAGS = $(shell $(WXCFG_BIN) --cxxflags | sed 's/-mthreads//g')
	# include for resource compilation!
	WX_VERS = $(shell $(WXCFG_BIN) --version)
	WX_MAJORVERS = wx-$(shell echo $(WX_VERS) | sed 's/^\([0-9]\.[0-9]\).*/\1/')
	WINDRES_FLAG = --include-dir $(XTOOL_DIR)/include
	WINDRES_FLAG += --include-dir $(XTOOL_DIR)/include/$(WX_MAJORVERS)
else
	BUILD_TOOL=gcc
	WX_LIBFLAGS = $(shell wx-config --libs $(WX_LIBS))
	WX_CXXFLAGS = $(shell wx-config --cxxflags)
endif
PACKDAT += $(GUISPRO)

CC = $(CROSS_COMPILE)gcc
CPP = $(CROSS_COMPILE)g++
RES = $(CROSS_COMPILE)windres
debug: LOCAL_FLAGS += -DMY1DEBUG
pack: ARCNAME = $(PACKDIR)-$(PLATBIN)-$(shell date +%Y%m%d%H%M)$(ARCHEXT)
version: VERSION = -DMY1APP_PROGVERS=\"$(shell cat VERSION)\"

all: gui

gui: $(GUISPRO)

new: clean all

debug: new

find:
	@echo "Tool:{$(BUILD_TOOL)}"

pack: all
	mkdir -pv $(PACKDIR)
	$(COPY) $(PACKDAT) $(PACKDIR)/
	$(DELETE) $(ARCNAME)
	$(ARCHIVE) $(ARCNAME) $(PACKDIR)

version: new pack

$(GUISPRO): $(GUISOBJ)
	$(CPP) $(CFLAGS) -o $@ $+ $(LFLAGS) $(OFLAGS) $(WX_LIBFLAGS)

wx%.o: src/wx%.cpp src/wx%.hpp
	$(CPP) $(CFLAGS) $(VERSION) $(WX_CXXFLAGS) $(LOCAL_FLAGS) -c $<

wx%.o: src/wx%.cpp
	$(CPP) $(CFLAGS) $(VERSION) $(WX_CXXFLAGS) $(LOCAL_FLAGS) -c $<

%.o: src/%.c src/%.h
	$(CC) $(CFLAGS) $(LOCAL_FLAGS) -c $<

%.o: src/%.c
	$(CC) $(CFLAGS) $(LOCAL_FLAGS) -c $<

%.o: src/%.cpp src/%.hpp
	$(CPP) $(CFLAGS) $(LOCAL_FLAGS) -c $<

%.o: src/%.cpp
	$(CPP) $(CFLAGS) $(LOCAL_FLAGS) -c $<

%.ico: res/%.xpm
	$(CONVERT) $< $@

%.res: src/%.rc apps.ico
	$(RES) --include-dir res $(WINDRES_FLAG) -O COFF $< -o $@

%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	$(CC) $(CFLAGS) -c $<

%.o: $(EXTPATH)/%.c
	$(CC) $(CFLAGS) -c $<

%.o: $(EX2PATH)/%.c $(EX2PATH)/%.h
	$(CC) $(CFLAGS) -c $<

%.o: $(EX2PATH)/%.c
	$(CC) $(CFLAGS) -c $<

wx%.o: $(EX3PATH)/wx%.cpp $(EX3PATH)/wx%.hpp
	$(CC) $(CFLAGS) $(WX_CXXFLAGS) -c $<

clean:
	-$(DELETE) $(PROJECT) $(PROJECT)-* *.exe *.bz2 *.zip *.o *.ico *.res

sweep: clean
	-$(DELETE) asm/*.lst asm/*.hex
